var markerPos = [47.22331, 8.81752];
var zoom = 18;

var map = L.map('map').setView(markerPos, zoom);

map.attributionControl.setPrefix(false);

L.tileLayer('http://tile.osm.ch/osm-swiss-style/{z}/{x}/{y}.png', {
    attribution: '<a id="problem-attribution" ' +
        `href="http://www.openstreetmap.org/fixthemap?lat=${markerPos[0]}&lon=${markerPos[1]}&zoom=${zoom}">Report a problem</a> | ` +
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
}).addTo(map);

var marker = new L.marker(markerPos).addTo(map);

marker.bindPopup("OST Campus RJ").openPopup();

map.on('move', function () {
    document.getElementById('problem-attribution').setAttribute('href', createProblemLink());
});

function createProblemLink() {
    const coords = map.getCenter();
    const zoom = map.getZoom();

    return `http://www.openstreetmap.org/fixthemap?lat=${coords.lat}&lon=${coords.lng}&zoom=${zoom}`;
}